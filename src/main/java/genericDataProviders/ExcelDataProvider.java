package genericDataProviders;

import genericLibraries.ExcelDriver;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import org.testng.annotations.DataProvider;

public class ExcelDataProvider 
{
	@DataProvider(name="DataProvider_Excel")
	public static Iterator<Object[]> excelDataProvider(Method m) throws IOException
	{
		List<Object[]> ls = null;
		//System.out.println("Get the name from DataProvider -> Reflector parameter " + m.getName());
		ls = ExcelDriver.getXlData(m.getName());
//		System.out.println(ls.size());
//		String lsOne = ls.get(0).toString();
//		System.out.println(lsOne);
		return ls.iterator();
	}
}
