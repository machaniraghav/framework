package genericDataProviders;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.DataProvider;

import genericLibraries.DatabaseDriver;

public class DatabaseDataProvider 
{
	@DataProvider(name = "DataProvider_DB")
	public static Iterator<Object[]>databaseDataProvider(Method m)
	{
		System.out.println("Get method name from DatabaseDataProvider Method reflector parameter " + m.getName());
		List<Object[]> ls = DatabaseDriver.getDataBaseData(m.getName());
		return ls.iterator();
	}
}
