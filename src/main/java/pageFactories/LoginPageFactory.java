package pageFactories;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageFactory 
{
	public LoginPageFactory(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "logid")
	public WebElement emailId;
	
	@FindBy(xpath = "//input[@type='password'][@name = 'pswd']")
	public WebElement password;
	
	@FindBy(css = "td.sb1>input")
	public WebElement loginBtn;
	
	@FindBy(xpath = "//b[contains(text(),'Sorry we were')]")
	public List<WebElement> invalidMsg1Sorry;
	
	@FindBy(xpath = "//b[contains(text(),'The username and')]")
	public List<WebElement> invalidMsg2EntryIncorrect;
	
	@FindBy(xpath = "//li")
	public List<WebElement> invalidMsg2FullMessage;
	
}
