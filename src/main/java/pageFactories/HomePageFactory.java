package pageFactories;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageFactory 
{
	public HomePageFactory(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//span[@id='sigin_info']/a")
	public WebElement signIn;
	
	@FindBy(css = "#username>a")
	public WebElement signInName;
	
	@FindBy(id = "srchword")
	public WebElement searchKeyWord;
	
	@FindBy(className = "newsrchbtn")
	public WebElement searchBtn;
	
	@FindBy(id = "find")
	public WebElement searchResultCount;
	
	@FindBy(css = ".sorrymsg")
	public List<WebElement> searchResultInvalidMessage;
}
