package genericLibraries;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigPropertyFileReader 
{
	static String configPropFilePath = System.getProperty("user.dir") 
							+ "\\src\\main\\resources\\propertyFiles\\Config.properties";
	
	public static String getConfigPropertyFileValues(String key)
	{
		String value = null;
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(configPropFilePath);
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
		Properties propFile = new Properties();
		try 
		{
			propFile.load(fis);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			try 
			{
				fis.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		value = propFile.getProperty(key);
		return value;
	}
	
	
}
