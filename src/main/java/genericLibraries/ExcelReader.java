package genericLibraries;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ExcelReader 
{
	XSSFWorkbook wb = null;
	
	//Connect to Excel through constructor
	public ExcelReader(String path) throws IOException 
	{
		
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(path);
			if(fis != null)
			{
				System.out.println("Connection Successful");
			}
			wb = new XSSFWorkbook(fis);
			
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		} 
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			fis.close();
		}
	}
	
	
	//get SheetName
	public XSSFSheet getSheetName(String sheetName)
	{
		XSSFSheet sheet = wb.getSheet(sheetName);
		return sheet;
	}
	
	//get RowCount
	public int getRowCount(String sheetName)
	{
		XSSFSheet sheet = getSheetName(sheetName);
		int rowCount = sheet.getLastRowNum();
		return rowCount + 1;
	}
	
	//getCell Count
	public int getCellCount(String sheetName)
	{
		XSSFSheet sheet = getSheetName(sheetName);
		int cellCount = sheet.getRow(0).getLastCellNum();
		return cellCount;
	}
	
	
	//getCell count for a particular Row
	public int getCellCountByRow(String sheetName, int rowIndex)
	{
		XSSFSheet sheet = getSheetName(sheetName);
		int cellCount = sheet.getRow(rowIndex).getLastCellNum();
		return cellCount;
	}
	
	//ReadExcelData based on SheetName, RowIndex and Cell Index
	public String readXlData(String sheetName, int rowIndex, int cellIndex)
	{
		XSSFSheet sheet = getSheetName(sheetName);
		//System.out.println("SheetName in Excel Reader -> ReadXlData = " + sheetName);
		String cellText = null;
		XSSFCell cell = sheet.getRow(rowIndex).getCell(cellIndex);
		
		if(cell.getCellTypeEnum() == CellType.BLANK)
		{
			cellText = "";
		}
		else if(cell.getCellTypeEnum() == CellType.STRING)
		{
			cellText = cell.getStringCellValue();
		}
		else if(cell.getCellTypeEnum() == CellType.NUMERIC)
		{
			double numericCellValue = cell.getNumericCellValue();
			cellText = String.valueOf(numericCellValue);
		}
		
		else if(cell.getCellTypeEnum() == CellType.BOOLEAN)
		{
			boolean b = cell.getBooleanCellValue();
			cellText = String.valueOf(b);
		}
		return cellText;
	}
	
	//Write to Excel based on SheetName, RowIndex, Column Index and the value to be written
	public void writeDataToExcel(String sheetName, int rowIndex, int cellIndex, String value)
	{
		XSSFSheet sheet = getSheetName(sheetName);
		XSSFRow row = sheet.getRow(rowIndex);
		
		if(row == null)
		{
			row = sheet.createRow(rowIndex);
		}
		
		XSSFCell cell = sheet.getRow(rowIndex).getCell(cellIndex);
		if(cell == null)
		{
			cell = row.createCell(cellIndex);
		}
		cell.setCellValue(value);	
	}
	
	//Save Data to Excel and close the Excel connection 
	public void saveXlData(String path) throws IOException 
	{
		FileOutputStream fos = null;
		try 
		{
			fos = new FileOutputStream(path);
			if(fos != null)
			{
				System.out.println("File Output Stream has connected Successfully");
				wb.write(fos);
				
			}
			
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			fos.close();
			wb.close();
		}
		
	}
}
