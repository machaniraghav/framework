package genericLibraries;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import genericLibraries.ConfigPropertyFileReader;

public class ExcelDriver 
{
	static String xlPath = System.getProperty("user.dir")
						+ ConfigPropertyFileReader.getConfigPropertyFileValues("resourcesPath") 
						+ ConfigPropertyFileReader.getConfigPropertyFileValues("testDataFilesPath")
						+ ConfigPropertyFileReader.getConfigPropertyFileValues("excelFileName");
	
	private static String sheetName = null;
	static String xlReferenceSheetName = ConfigPropertyFileReader.getConfigPropertyFileValues("excelReferenceSheetName");
	private static ExcelReader xl = null;
	
	private static String getSheetName(String methodName)
	{
		//get row count of the Reference Sheet
		int rowCountRef = xl.getRowCount(xlReferenceSheetName);
		for(int i = 1; i<rowCountRef; i++)
		{
			//get the names of all the methods one at a time in the methodcolumn of excel
			String methodColumnData = xl.readXlData(xlReferenceSheetName, i, 1);
			if(methodColumnData.equalsIgnoreCase(methodName))
			{
				//retrieve the sheetname corresponding to the methodName of excel
				sheetName = xl.readXlData(xlReferenceSheetName, i, 2);
				break;
			}
			
		}
		return sheetName;
	}
	
	
	//Retrieve excel data values and put it in List<Object[]> so as to be consumed by Data Provider
	//@SuppressWarnings("null")
	public static List<Object[]> getXlData(String methodName) throws IOException 
	{
		
		xl = new ExcelReader(xlPath);
		Map<String, String> map = null;
		Object[] obj = null;	
		List<Object[]> ls = new ArrayList<Object[]>();
		
		sheetName = getSheetName(methodName);
		int rowCount = xl.getRowCount(sheetName);
		int cellCount = xl.getCellCount(sheetName);
		
		for(int i=1; i<rowCount; i++)
		{
			map = new HashMap<String, String>();
			obj = new Object[1]; 
			//check for the execution status
			String executionStatus = xl.readXlData(sheetName, i, 3);
			if(executionStatus.equalsIgnoreCase("Y"))
			{
				
				for(int j=0; j<cellCount; j++)
				{
					//dump entire row of excel one column at a time as Map<key,value>
					String key = xl.readXlData(sheetName, 0, j);
					String value = xl.readXlData(sheetName, i, j);
					map.put(key, value);
				}
				//add map to object[] and then to List so as to format the data retrieved in the form of List<Object[]>
				obj[0] = map;
				ls.add(obj);
//				for(Object[] o : ls)
//				{
//					o.getClass();
//				}
//				
			}
		}
		return ls;
	}
}
