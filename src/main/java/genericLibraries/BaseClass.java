package genericLibraries;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
//import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import genericLibraries.ConfigPropertyFileReader;

public class BaseClass 
{
	public WebDriver driver = null;
	String browserType = null;
	
	public static ExtentReports erReports = null;
	public static ExtentTest etLogger = null;
	public static String curTestCaseName = null;
	
	private String extentReportPath = "\\test-output\\ExtentReport";
	private String path = System.getProperty("user.dir") + extentReportPath + "\\extentReport.html";
	private String TC_ID = null;
	private String Run = null;
	private String Functionality = null;
	private String testCaseName = null;
	
	//Reading from Config.properties file to retrieve the corresponding values
	String url = ConfigPropertyFileReader.getConfigPropertyFileValues("URL");
	String implicitWait = ConfigPropertyFileReader.getConfigPropertyFileValues("ImplicitWait");
	long implicitWaitTime = Long.parseLong(implicitWait);
	
	String pageLdTimeOut = ConfigPropertyFileReader.getConfigPropertyFileValues("pageLoadTimeOut");
	long pageLoadTimeOut = Long.parseLong(pageLdTimeOut);
	
	String explicitWait = ConfigPropertyFileReader.getConfigPropertyFileValues("explicitWaitTimeOut");
	long explicitWaitTimeOut = Long.parseLong(explicitWait);
	
	private String resourcesPath = ConfigPropertyFileReader.getConfigPropertyFileValues("resourcesPath");
	String propertyFilesPath = ConfigPropertyFileReader.getConfigPropertyFileValues("propertyFilesPath");
	String testDataFilesPath = ConfigPropertyFileReader.getConfigPropertyFileValues("testDataFilesPath");
	String webDriverFilesPath = ConfigPropertyFileReader.getConfigPropertyFileValues("webDriverFilesPath");
	String xmlFilesPath = ConfigPropertyFileReader.getConfigPropertyFileValues("xmlFilesPath");
	String chromeDriver = ConfigPropertyFileReader.getConfigPropertyFileValues("chromeDriverName");
	String geckoDriver = ConfigPropertyFileReader.getConfigPropertyFileValues("geckoDriverName");
	String edgeDriver = ConfigPropertyFileReader.getConfigPropertyFileValues("EdgeDriverName");
	
	@BeforeSuite
	public void initializeVariables()
	{
		erReports = new ExtentReports(path,true);		
	}
	
	@BeforeMethod
	@Parameters("browsertype")
	public void initializeBrowser(@Optional("chrome") String browser, Object[] obj)
	{
		Map<String, String> map = getTestInfo(obj);
		
		browserType = browser;
//		browserType = "chrome";
		
		TC_ID = map.get("TC_ID");
		Run = map.get("Run");
		Functionality = map.get("Functionality");
		
//		TC_ID = "TestCase1";
//		Run = "1";
//		Functionality = "CheckBrowser";
		testCaseName = TC_ID+"_"+Run+"_"+Functionality;
		
		
		etLogger = erReports.startTest(testCaseName);
		etLogger.assignCategory(browserType);
		etLogger.log(LogStatus.INFO, "Initializing the Browser", "Successful");
		if(browserType.equalsIgnoreCase("firefox"))
		{
//			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
//					+"\\src\\test\\resources\\webDrivers\\geckodriver.exe");
			
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
					+ resourcesPath + webDriverFilesPath + geckoDriver);
			driver = new FirefoxDriver();
		}
		
		else if(browserType.equalsIgnoreCase("chrome"))
		{
//			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
//					+"\\src\\main\\resources\\webDrivers\\chromedriver.exe");
			
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
					+ resourcesPath + webDriverFilesPath + chromeDriver);
			driver = new ChromeDriver();
		}
		if(!browserType.equalsIgnoreCase("firefox"))
		{
			driver.manage().window().maximize();
		}
		driver.get(url);
//		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(pageLoadTimeOut, TimeUnit.SECONDS);
		
//		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(implicitWaitTime,TimeUnit.SECONDS);
		
		
	}
		
//		@AfterMethod
//		public void tearDown(ITestResult result)
//		{
//			LogStatus erRunStatus = etLogger.getRunStatus();
//			System.out.println(erRunStatus.toString());
//			curTestCaseName = result.getName();
//			if(result.getStatus() == ITestResult.FAILURE)
//			{
//				String screenShot = getScreenShot();
//				etLogger.log(LogStatus.FAIL, "The testcase " + curTestCaseName + " has failed", 
//						     etLogger.addScreenCapture(screenShot));
//			}
//			else if(result.getStatus() == ITestResult.SUCCESS)
//			{
//				etLogger.log(LogStatus.PASS, "The test case " + curTestCaseName + " has passed");
//			}
//			
//			else if(result.getStatus() == ITestResult.SKIP)
//			{
//				etLogger.log(LogStatus.SKIP, curTestCaseName, "The testcase has skipped as expected");
//			}
//			driver.quit();
//			erReports.endTest(etLogger);
//		} 
		
		@AfterMethod
		public void tearDown(ITestResult result)
		{
			LogStatus erRunStatus = etLogger.getRunStatus();
			System.out.println(erRunStatus.toString());
			
			curTestCaseName = result.getName();
			if(erRunStatus == LogStatus.FAIL)
			{
				String screenShot = getScreenShot();
				etLogger.log(LogStatus.FAIL, "The testcase " + curTestCaseName + " has failed", 
						     etLogger.addScreenCapture(screenShot));
			}
			else if(erRunStatus == LogStatus.PASS)
			{
				etLogger.log(LogStatus.PASS, "The test case " + curTestCaseName + " has passed");
			}
			
			else if(erRunStatus == LogStatus.SKIP)
			{
				etLogger.log(LogStatus.SKIP, curTestCaseName, "The testcase has skipped as expected");
			}
			driver.quit();
			erReports.endTest(etLogger);
		} 
		
	
	
	@AfterSuite
	public void endSuite()
	{
		//append report result to the html file
		//can be invoked multiple times
		erReports.flush();
		
		//close all underlying connections 
		//invoked only once per execution
		erReports.close();
	}
	
	
	
	private String getScreenShot() 
	{
//		String testName = tcName;
//		System.out.println("To string of etLogger.getTest() = " + testName);
		Date date = new Date();
		
		//hh:mm:ss format will generate IOException as the folder naming convention does not allow ":" characters
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");
		String timeStamp = simpleDateFormat.format(date);
		
		//format the screenshot name 
		//String screenShotName = TC_ID+"_"+Run+"_"+Functionality+"_"+timeStamp+".png";
		String screenShotName = TC_ID+"_"+timeStamp+".png";
//		String path = System.getProperty("user.dir")+extentReportPath + "/Screenshots/"
//								+ testCaseName + "/" + screenShotName;
		
		String path = System.getProperty("user.dir") + extentReportPath + "\\Screenshots\\" + screenShotName;
								
		
		TakesScreenshot screenShot = (TakesScreenshot) driver;
		File screenShotAs = screenShot.getScreenshotAs(OutputType.FILE);
		
		try
		{
			FileUtils.copyFile(screenShotAs, new File(path));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return path;
	}

//	public void launchURL(String url)
//	{
//		driver.get(url);
//		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
//		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
//	}
	
	
	public Map<String, String> getTestInfo(Object[] obj) 
	{
		
		@SuppressWarnings("unchecked")
		Map<String, String> map = (Map<String, String>) obj[0];
		return map;
	}
	
	//to skip the execution in case of unintended interaction with the application
	public static void skipExecution(String message)
	{
		throw new SkipException(message);
	}
	
	//force stop the execution to maintain the test integrity 
	public static void forceStopExecution(String message) throws Exception
	{
		throw new Exception(message);
	}
	
//	@Test
//	public void testScreenshot()
//	{
//		String s = getScreenShot();
//		System.out.println(s);
//	}
}
