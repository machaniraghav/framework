package testScripts;
import java.util.Map;

import org.apache.log4j.Logger;
//import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import genericLibraries.BaseClass;
import pages.LoginPage;

public class LoginScenario extends BaseClass
{
	Logger log = Logger.getLogger(LoginScenario.class);
	
	@Test(dataProvider = "DataProvider_Excel", dataProviderClass = genericDataProviders.ExcelDataProvider.class)
	public void validLoginScenario(Map<String, String> map)
	{
		log.info("User is in Home Page");
		BaseClass.etLogger.log(LogStatus.INFO, "User is in the Home Page", "Successful");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginModule(map);
		//log.info("User has successfully logged in");
		
	}
}
