package pages;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.log4testng.Logger;

import com.relevantcodes.extentreports.LogStatus;

import pageFactories.HomePageFactory;
import pageFactories.LoginPageFactory;
import genericLibraries.BaseClass;
import junit.framework.Assert;

public class LoginPage 
{
	WebDriver driver = null;
	Logger log = Logger.getLogger(LoginPage.class);
	HomePageFactory homePage = null;
	LoginPageFactory loginPage = null;
	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public void loginModule(Map<String, String> map)
	{
		String emailId = map.get("UserName");
		String password =  map.get("Password");
		
		homePage = new HomePageFactory(driver);
		loginPage = new LoginPageFactory(driver);
		
		homePage.signIn.click();
		log.info("User has navigated to Login Page");
		
		BaseClass.etLogger.log(LogStatus.INFO, "User is navigated to Login Page", "Successful");
		
		loginPage.emailId.sendKeys(emailId);
		loginPage.password.sendKeys(password);
		loginPage.loginBtn.click();
		
		log.info("User Entered login details and clicked on Login Button");
		BaseClass.etLogger.log(LogStatus.INFO, "User Entered login details and clicked on Login Button", "Successful");
		
		if(loginPage.invalidMsg1Sorry.size() > 0)
		{
//			System.out.println("The testcase has to be failed");
//			log.warn("The testcase has to be skipped because the login credentials are incorrect");
//			
//			BaseClass.etLogger.log(LogStatus.WARNING, "The credentials are incorrect, skipping the execution for "
//					+ "this test case.Check the log for more info", "Successfull");
//			BaseClass.skipExecution("The user has given incorrect user details");
			
			System.out.println("The testcase has to be failed");
			log.fatal("The testcase has to be failed because the login credentials are incorrect");
			BaseClass.etLogger.log(LogStatus.FAIL, "The testcase has to be failed because the login credentials are incorrect" 
									, "Failed");
			
//			System.out.println("InvalidMsg1Sorry Message = " + loginPage.invalidMsg1Sorry.toString());
//			BaseClass.etLogger.log(LogStatus.INFO, "InvalidMsg1Sorry Message = " + loginPage.invalidMsg1Sorry.toString());
//			
//			System.out.println("InvalidMsg2Incorrect Entry Message = " 
//					+ loginPage.invalidMsg2EntryIncorrect.toString());
//			BaseClass.etLogger.log(LogStatus.INFO, "InvalidMsg2Incorrect Entry Message = " 
//								+ loginPage.invalidMsg2EntryIncorrect.toString());
//			
//			System.out.println("InvalidMsg2Full Message = " + loginPage.invalidMsg2FullMessage.toString());
//			BaseClass.etLogger.log(LogStatus.INFO, "InvalidMsg2Full Message = " + loginPage.invalidMsg2FullMessage.toString());
			
		}
		else
		{
			String actual = homePage.signInName.getText();
			String expected = map.get("AccountID");
			
			Assert.assertEquals(expected, actual);
			log.info("The user has logged in Successfully.");
			BaseClass.etLogger.log(LogStatus.INFO, "The user has logged in Successfully", "Successful");
		}
		
		
	}
}
