package pages;

import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import genericLibraries.BaseClass;
import junit.framework.Assert;
import pageFactories.HomePageFactory;

public class HomePage 
{
	WebDriver driver = null;
	HomePageFactory homePage = null;
	Logger log = Logger.getLogger(HomePage.class);
	public HomePage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public void getSearchBookCount(Map<String, String> map)
	{
		String searchWord = map.get("SearchKeyword");
		homePage = new HomePageFactory(driver);
		log.info("Start book search in the Home Page");
		homePage.searchKeyWord.sendKeys(searchWord);
		homePage.searchBtn.click();
		log.info("User has entered the book " + searchWord 
					+ " and clicked on the Search Button");
		
		//If the search result was unsuccessful
		if(homePage.searchResultInvalidMessage.size() > 0)
		{
			System.out.println("Search keyword did not return any result " + searchWord);
			log.warn("<LoggerWarn>: Search keyword did not return any result " + searchWord);
			BaseClass.skipExecution("Search keyword " + searchWord + " did not yield successful result count");
			
		}
		else
		{
			String actual = homePage.searchResultCount.getText();
			log.info("Actual Search Count = " + actual);
			String expected = map.get("BookNo.");
			log.info("Expected Search Count = " + expected);
			Assert.assertEquals(expected, actual);
			
		}
		
		
	}
	
}
